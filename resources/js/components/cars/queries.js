import gpl from 'graphql-tag';

export const CARSBYBRAND_QUERY = gpl`
query($brand_id: Int){ 
    cars_by_brand(brand_id:$brand_id){ 
     id
     name
     description
     release_date
     brand_id
     brand{
       name
     }
   
   }
 }

`;
