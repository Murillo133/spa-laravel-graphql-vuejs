import gpl from "graphql-tag";


export const CREATE_CAR = gpl`
mutation($car: CarInput!) {
    createCar(input: $car){
        id
    }
}
`;

export const DELETE_CAR = gpl`
mutation($id: ID!) {
    deleteCar(id: $id){
        id
    }
}
`;

export const UPDATE_CAR= gpl`
mutation($id:ID!,$car: CarInput!) {
    updateCar(id:$id, input: $car){
        id
    }
}
`;