import gpl from "graphql-tag";


export const CREATE_BRAND = gpl`
mutation($brand: BrandInput!) {
    createBrand(input: $brand){
        id
        name
        fundation_date
        country
    }
}
`;

export const DELETE_BRAND = gpl`
mutation($id: ID!) {
    deleteBrand(id: $id){
        id
    }
}
`;

export const UPDATE_BRAND = gpl`
mutation($brand: UpdateBrandInput!) {
    updateBrand(input: $brand){
        id
    }
}
`;