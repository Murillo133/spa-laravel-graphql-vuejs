import gpl from "graphql-tag";

export const BRANDS_QUERY = gpl`
query {
    brands{
      paginatorInfo {
        hasMorePages
      }
      data {
        id
        name
        fundation_date
        country
      }
    }
  }
`;
