import Vue from "vue";
import VueRouter from "vue-router";

import Brands from "./components/brands/index.vue";
import Cars from "./components/cars/index.vue";

Vue.use(VueRouter);

const router = new VueRouter(
    {
        mode: "history",
        linkExactActiveClass: "active",

        routes: [{
            path: "/brands",
            name:"brands",
            component: Brands
        },
        {
            path: "/cars/:brand_id",
            name:"cars",
            component: Cars
        }
    ]
}
);

export default router;
