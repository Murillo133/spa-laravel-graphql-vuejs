# Crear proyecto laravel

    composer create-project --prefer-dist laravel/laravel nombre
    Una vez se crea el proyecto:

        cd nombre
        Generamos la llave
        php artisan key:generate
    
## Agregar seguridad:

        composer require laravel/ui

        php artisan ui vue --auth
        
        npm install
		npm run dev
        
## Agregar almacenamiento publico a laravel
    	
      	php artisan storage:link
        
        Para hacer referencia a datos de este
        almacenamiento se utiliza:
        public_path("ruta")
    
## Agregar GraphQL para crear la API
    
    composer require nuwave/lighthouse
    
## Agregar rutas a contralodores en app desde grphQL

    Con estas rutas podemos enviar datos de graphQl
    a App de laravel en caso de requerir funciones de controlador No lo necesitamos
    
    php artisan lighthouse:mutation NombreControlador


# Agregar VueJs
    
    composer require laravel/ui
    php artisan ui vue
      
    php artisan make:controller SpaController

      	
>Dentro del controlador colocamos la función:
```php 
   public function index()
    {
        return view('home');
    }
```     	
# Instalar Apollo client como herramienta de consultas a la API de GraphQL

```php 
    npm install vue-apollo
    npm install vue-router
    npm install apollo-boost
    npm instal graphql
    npm instal graphql-tag
```
## Crear controlador SPA para poder ver la vista principal

    En home.blade remplazar con el siguiente código:

```php
    @extends('layouts.app')

    @section('content')
    <div class="container-fluid">
        <router-view></router-view>
    </div>
    @endsection
```
# Crear los modelos necesarios para el taller

    php artisan make:model Brand -m

    php artisan make:model Car -m

>Para cada uno de los modelos se crean las relaciones de base de datos, donde una marca tiene muchos carros y muchos carros tiene una marca.


# Creamos los esquemas de la API que conectan con la base de datosdentro de Vehicles/graphql 

>para el usuario user.graphql

```graphql
    extend type Query {
    users: [User!]! @paginate(defaultCount: 10)
    user(id: ID @eq): User @find
}

type User {
    id: ID!
    name: String!
    email: String!
    created_at: DateTime!
    updated_at: DateTime!
}
```
>para la marca brand.graphql

```graphql
#Conecta a la base de datos según el modelo y facilita la búsqueda de todos y para uno en este caso, si se necesitan más filtros, se creand dentro del Query
extend type Query {
    brands: [Brand!]! @paginate(defaultCount: 12)
    brand(id: ID @eq): Brand @find
}
#Funciones que son llamadas desde apollo que reciben los datos según la acción CREATE UPDATE DELETE 
extend type Mutation {
    createBrand(input: BrandInput! @spread): Brand! @create
    updateBrand(input: UpdateBrandInput! @spread): Brand! @update
    deleteBrand(id: ID!): Brand! @delete
}
#Modelos, es importante definir la relación con los otros modelos
type Brand {
    id: ID!
    name: String!
    fundation_date: DateTime!
    country: String!
    cars: [Car!]! @hasMany
    created_at: DateTime!
    updated_at: DateTime!
}
#Registro
input BrandInput {
  name: String! @rules(apply: ["required"])
  fundation_date: Date! @rules(apply: ["required"])
  country: String! @rules(apply: ["required"])
}
#Actualización
input UpdateBrandInput{
  id: ID!
  name: String! @rules(apply: ["required"])
  fundation_date: Date! @rules(apply: ["required"])
  country: String! @rules(apply: ["required"])
}

```
>para el de carros car.graphql

```graphql
extend type Query {
    cars: [Car!]! @paginate(defaultCount: 12)
    car(id: ID @eq): Car @find
    cars_by_brand(brand_id: Int @eq): [Car!]! @all
}

extend type Mutation {
    createCar(input: CarInput! @spread): Car! @create
    updateCar(id: ID!, input: CarInput! @spread): Car! @update
    deleteCar(id: ID!): Car! @delete
}

type Car {
    id: ID!
    name: String!
    description: String!
    release_date: DateTime!
    brand_id: Int!
    brand: Brand @hasOne
    created_at: DateTime!
    updated_at: DateTime!
}

input CarInput {
  name: String! @rules(apply: ["required"])
  description: String! @rules(apply: ["required"])
  release_date: Date! @rules(apply: ["required"])
  brand_id: Int! @rules(apply: ["required"])
}

input UpdateCarInput{
  id: ID!
  name: String! @rules(apply: ["required"])
  description: String! @rules(apply: ["required"])
  fundation_date: Date! @rules(apply: ["required"])
}
```

>Dentro de schema.graphql:

```graphql
"A date string with format `Y-m-d`, e.g. `2011-05-23`."
scalar Date @scalar(class: "Nuwave\\Lighthouse\\Schema\\Types\\Scalars\\Date")

"A datetime string with format `Y-m-d H:i:s`, e.g. `2018-05-23 13:43:32`."
scalar DateTime @scalar(class: "Nuwave\\Lighthouse\\Schema\\Types\\Scalars\\DateTime")
## aquí instanciamos nuestros esquemas para poder acceder a los Queries y Mutations que nos permiten manejar los datos de la página
type Query
type Mutation

#import user.graphql
#import brand.graphql
#import car.graphql

```

## Hacemos las migraciones para probar la API

    php artisan config:cache
    npm run production
    php artisan migrate


# Conectando la API con vueJS para obtener las funcionalidades SPA

    Lo primero es crear nuestros componentes dentro de resources/js/components

    Creamos la carpeta de componentes brands(Para manejar las vistas de las marcas y las acciones de CRUD)
    Dentro creamos nuestro archivo brands/index.vue
    Colocamos una estiqueta:
    <template>Aquí va todo nuestro HTML asociado a v-models</template> 
   <script> 
   Aquí dentro va todo nuestro código javascript que nos permite manejar los datos
   Dentro de este script lo primero que se debe hacer es importar los componentes con los que se va a relacionar la vista, en este caso, se debe importar el index de los carros relacionados a la marca, que de igual manera se creará una carpeta dentro de components/cars/index.vue.
   
   Dentro se importan los queries y mutations que nos permiten mediante apollo client conectar a la API de GraphQl para manejar la información, esto archivos de queries y mutations se deben crear junto al archivo de index para cada carpeta de componentes.

   Definimos las variables a utilizar, así como los modelos virtuales con sus variables dentro de la función data.

   Por último, tenemos la función methods, dentro de esta creamos toda la lógica necesaria para el funcionamiento de la SPA.

   </script>



>Ejemplo de los archivos en la carpeta de brands
>>index.vue
```javascript
<script>
import Car from "../cars/index";
import {
    ValidationProvider,
    ValidationObserver
} from 'vee-validate';
import {
    BRANDS_QUERY
} from "./queries.js";
import {
    CREATE_BRAND,
    DELETE_BRAND,
    UPDATE_BRAND
} from "./mutations.js";

export default {
    components: {
        ValidationProvider,
        ValidationObserver,
        Car
    },
    apollo: {
        brands: {
            query: BRANDS_QUERY
        }
    },
    data() {
        return {
            show_car: false,
            brand_id: 0,
            newBrand: {
                name: "",
                fundation_date: "",
                country: ""
            },
            updateBrand: {
                id: null,
                name: "",
                fundation_date: "",
                country: ""
            },
            show_form: false,
            showButtonSave: true
        };
    },
    methods: {

        hideCars(){
            this.show_car = false;
        },
        car(id) {
            this.brand_id = id;
            this.show_car = true;
        },
        reset() {
            this.show_form = false;
            this.showButtonSave = true;
            this.newBrand.name = "";
            this.newBrand.fundation_date = "";
            this.newBrand.country = "";
            this.updateBrand.name = "";
            this.updateBrand.fundation_date = "";
            this.updateBrand.country = "";
        },
        remove(id) {
            if (confirm("Are you sure to delete this Brand?")) {
                this.$apollo
                    .mutate({
                        mutation: DELETE_BRAND,
                        variables: {
                            id: id
                        }
                    })
                    .then(response => {
                        this.$apollo.queries.brands.refetch();
                        this.reset();
                    });
            }
        },
        save() {
            this.$apollo
                .mutate({
                    mutation: CREATE_BRAND,
                    variables: {
                        brand: this.newBrand
                    }
                })
                .then(response => {
                    this.$apollo.queries.brands.refetch();
                    this.reset();
                });

        },
        update() {
            if (confirm("Are you sure?")) {
                (this.updateBrand.name = this.newBrand.name),
                (this.updateBrand.fundation_date = this.newBrand.fundation_date),
                (this.updateBrand.country = this.newBrand.country);
                this.$apollo
                    .mutate({
                        mutation: UPDATE_BRAND,
                        variables: {
                            brand: this.updateBrand
                        }
                    })
                    .then(response => {
                        this.$apollo.queries.brands.refetch();
                        this.reset();
                    });
            }
        },
          backList(){
           this.show_car = false;
        },
        edit(brand) {
            this.showButtonSave = false;
            this.show_form = true;
            (this.updateBrand.id = brand.id),
            (this.newBrand.name = brand.name),
            (this.newBrand.fundation_date = brand.fundation_date.split(" ", 1)[0]),
            (this.newBrand.country = brand.country);
        }
    }
};
</script>

```
>>mutations.js
```javascript
import gpl from "graphql-tag";


export const CREATE_BRAND = gpl`
mutation($brand: BrandInput!) {
    createBrand(input: $brand){
        id
        name
        fundation_date
        country
    }
}
`;

export const DELETE_BRAND = gpl`
mutation($id: ID!) {
    deleteBrand(id: $id){
        id
    }
}
`;

export const UPDATE_BRAND = gpl`
mutation($brand: UpdateBrandInput!) {
    updateBrand(input: $brand){
        id
    }
}
`;
```

>>queries.js
```javascript

import gpl from "graphql-tag";

export const BRANDS_QUERY = gpl`
query {
    brands{
      paginatorInfo {
        hasMorePages
      }
      data {
        id
        name
        fundation_date
        country
      }
    }
  }
`;

```
# Creando las rutas de la SPA

    Una vez creamos nuestros directorios con los componentes necesarios, debemos ir al archivo compnents/router.js que tendrá el siguiente código:

```js
import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const router = new VueRouter(
    {
        mode: "history",
        linkExactActiveClass: "active",

        routes: [
    ]
}
);
```
    A este le debemos indicar mediante un import las rutas de los archivos index para cada uno de los componentes


```js
import Vue from "vue";
import VueRouter from "vue-router";
//Aquí importamos el index para cada compnente
import Brands from "./components/brands/index.vue";
import Cars from "./components/cars/index.vue";

Vue.use(VueRouter);

const router = new VueRouter(
    {
        mode: "history",
        linkExactActiveClass: "active",
//Aquí creamos el alias de la ruta para cada componente mediante la variable path
//ya estas rutas estarán disponibles para ser utilizadas desde la vista principal de blade gracias al SPA controller que se creó durante la instalación de vueJS
        routes: [{
            path: "/brands",
            component: Brands
        },
        {
            path: "/cars",
            component: Cars
        }
    ]
}
);
``` 
# Configurar components/app.js

    Por último, en nuestro archivo app configuramos todo lo requerido para que nuestro sitio funcione, en este caso ya se configuró para hacer uso de boostrap en la instefaz, también se importaron las librerias de apollo y se configuró para hacer uso de este, quedando así:
```js

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import { required, email, numeric } from 'vee-validate/dist/rules';
import { extend } from 'vee-validate';

extend('required', {
  ...required,
  message: 'This field is required'
});
extend('email', {
  ...email,
  message: 'Invalid email'
});
extend('numeric', {
  ...numeric,
  message: 'Invalid number'
});

import router from "./router";
import VueApollo from 'vue-apollo';
import ApolloClient from 'apollo-boost';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

const httpLink = createHttpLink({
    // You should use an absolute URL here
    uri: 'http://localhost:8000/graphql',
});

const cache = new InMemoryCache()
const apolloClient = new ApolloClient({
    link: httpLink,
    cache,
});

Vue.use(VueApollo)

const apolloProvider = new VueApollo({
    defaultClient: apolloClient
});


const app = new Vue({
    el: '#app',
    router,
    apolloProvider
});

```

# Configurar el Home de blade

    Al ingresar a la aplicación nos encontraremos conlas vista welcome de laravel, una vez iniciemos sesión en nuestra aplicación, laravel nos redirigirá a su archivo home.blade.php, para que nos redirija a la vista principal de vueJS donde está toda la funcionalidad nos vamos a app/Http/Controllers/Auth/LoginController.php

    Aquí encontraremos la siguiente variable:
        
        protected $redirectTo = RouteServiceProvider::HOME;

    La cambiamos por esto:
        protected $redirectTo = RouteServiceProvider::BRANDS;
    
    Una vez cambiada nos vamos a definir esa b¿variable BRANDS a:

        app/Providers/RouteServiceProvider.php

    Estando en service provider vamos a encontrar la siguiente variable:

        public const HOME = '/home';
    
    La cambiamos por esto:

        public const BRANDS = '/brands';

    Para que todo funcione, la ruta '/brands' debe estar definida dentro del archivo router.js y apuntando a un index.vue en:

        resources/js/router.js

# Probando la SPA

## Contenedor de la SPA

    Se hace uso del archivo home.blade.php de laravel como contenedor para cargar todos los componentes de vuejs, mediante el siguiente código: 

```js
@extends('layouts.app')
@section('content')
<div class="container-fluid">
    Cada ruta solicitada carga la vista aquí
    <router-view></router-view>
</div>
@endsection
 ```

## En la SPA se pueden usar rutas diferentes para ver los contenidos

    También se pueden cambiar las vistas con el uso de props, pero no se ve el cambio en la URL, con la url se puede ir directamente a una vista de manera más rápida.

## Se actualiza el contenido necesario y no toda la página

## Actualización de datos sin refrescar 
## Peticiones a la API frontend independiente


