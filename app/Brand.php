<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['name', 'fundation_date', 'country'];

    public function cars()
    {
        return $this->hasMany('App\Car');
    }
}
