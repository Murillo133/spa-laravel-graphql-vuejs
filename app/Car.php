<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = ['name', 'description', 'release_date', 'brand_id'];

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
}
